# KICS analyzer

GitLab Analyzer for Infrastructure as Code (IaC) projects that calls [kics](https://github.com/Checkmarx/kics).
This analyzer is written in Go using the [command](https://gitlab.com/gitlab-org/security-products/analyzers/command) library shared by all analyzers.

The [common](https://gitlab.com/gitlab-org/security-products/analyzers/common) library documents how to run, test and modify this analyzer.

## Versioning and release process

Please check the [Release Process documentation](https://about.gitlab.com/handbook/engineering/development/secure/release_process.html).

## Updating the underlying Scanner

Please check the
[Static Analysis - We Own What We Ship documentation](https://about.gitlab.com/handbook/engineering/development/secure/static-analysis/#we-own-what-we-ship).

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the MIT Expat license, see the [LICENSE](LICENSE) file.
